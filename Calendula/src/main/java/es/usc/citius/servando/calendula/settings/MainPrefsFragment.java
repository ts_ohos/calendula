package es.usc.citius.servando.calendula.settings;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import es.usc.citius.servando.calendula.CalendulaActivity;
import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.util.LogUtil;

public class MainPrefsFragment extends PreferenceFragmentCompat {
    private final String TAG = "MainPrefsFragment";

    @Override
    public void onCreatePreferences(Bundle bundle, String rootKey) {
        LogUtil.d(TAG, "onCreatePreferences called");
        setPreferencesFromResource(R.xml.pref_main, rootKey);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CalendulaActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_activity_settings);
    }
}
