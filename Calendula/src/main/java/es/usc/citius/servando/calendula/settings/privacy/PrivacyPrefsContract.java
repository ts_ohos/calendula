package es.usc.citius.servando.calendula.settings.privacy;

import android.content.Intent;
import android.support.annotation.StringRes;

import es.usc.citius.servando.calendula.mvp.IPresenter;
import es.usc.citius.servando.calendula.mvp.IView;

public interface PrivacyPrefsContract {
    interface View extends IView {

        void recordPIN();

        void verifyPIN(int requestCode);

        void showPINOptions();

        void showConfirmDeletePinChoice();

        void setPINPrefText(@StringRes int pinPrefText);

        void setFingerprintPrefEnabled(boolean enabled);

        void setPINDependentPrefsEnabled(boolean enabled);

        void showEnableFingerprintDialog();
    }

    interface Presenter extends IPresenter<View> {

        void onResult(int request, int result, Intent data);

        void onClickPINPref();

        void onClickDeletePIN();

        void onClickModifyPIN();

        void confirmDeletePIN();
    }
}
