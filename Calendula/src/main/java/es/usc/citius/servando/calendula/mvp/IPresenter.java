package es.usc.citius.servando.calendula.mvp;

/**
 * Interface for MVP presenters
 */
public interface IPresenter<V extends IView> {

    /**
     * Initialization logic for the view. Load default values, etc... Android views should call this on onResume().
     */
    void start();

    /**
     * Notify view attachment to the presenter
     */
    void attachView(V view);

    /**
     * Notify view detachment to the presenter
     */
    void detachView();
}
