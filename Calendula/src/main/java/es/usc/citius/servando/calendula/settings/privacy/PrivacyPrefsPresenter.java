package es.usc.citius.servando.calendula.settings.privacy;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.VisibleForTesting;

import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.mvp.BasePresenter;
import es.usc.citius.servando.calendula.pinlock.PINManager;
import es.usc.citius.servando.calendula.pinlock.PinLockActivity;
import es.usc.citius.servando.calendula.pinlock.fingerprint.FingerprintHelper;
import es.usc.citius.servando.calendula.util.LogUtil;

public class PrivacyPrefsPresenter extends BasePresenter<PrivacyPrefsContract.View>
        implements PrivacyPrefsContract.Presenter {

    private final String TAG = "PrivacyPrefsPresenter";
    @VisibleForTesting
    public static final int REQUEST_MODIFY = 12152;
    @VisibleForTesting
    public static final int REQUEST_DELETE = 12318;

    private FingerprintHelper fpHelper;

    public PrivacyPrefsPresenter(FingerprintHelper fpHelper) {
        this.fpHelper = fpHelper;
    }

    @Override
    public void start() {
        if (PINManager.isPINSet()) {
            view.setPINPrefText(R.string.pref_summary_pin_lock_set);
            if (fpHelper.canUseFingerPrint()) {
                view.setFingerprintPrefEnabled(true);
            }
            view.setPINDependentPrefsEnabled(true);
        } else {
            view.setPINPrefText(R.string.pref_summary_pin_lock_unset);
        }
    }

    @Override
    public void onResult(int request, int result, Intent data) {
        LogUtil.d(
                TAG,
                "onResult() called with requestCode=$request, result=$result, data=$data"
        );

        if (result == Activity.RESULT_OK && data != null) {
            switch (request) {
                case PinLockActivity.REQUEST_PIN:
                    String pin = data.getStringExtra(PinLockActivity.EXTRA_NEW_PIN);
                    boolean pinManagerResult = PINManager.savePIN(pin);
                    if (pinManagerResult) {
                        view.setPINPrefText(R.string.pref_summary_pin_lock_set);
                        if (fpHelper.canUseFingerPrint()) {
                            view.setFingerprintPrefEnabled(true);
                            view.showEnableFingerprintDialog();
                        }
                        view.setPINDependentPrefsEnabled(true);
                    }
                    break;
                case REQUEST_DELETE:
                    if (data.getBooleanExtra(PinLockActivity.EXTRA_VERIFY_PIN_RESULT, false)) {
                        deletePIN();
                    }
                    break;
                case REQUEST_MODIFY:
                    if (data.getBooleanExtra(PinLockActivity.EXTRA_VERIFY_PIN_RESULT, false)) {
                        modifyPIN();
                    }
                    break;
            }
        } else {
            LogUtil.w(TAG, "onResult: invalid result or missing data");
        }
    }

    @Override
    public void onClickPINPref() {
        if (PINManager.isPINSet()) {
            view.showPINOptions();
        } else {
            view.recordPIN();
        }
    }

    @Override
    public void onClickDeletePIN() {
        view.showConfirmDeletePinChoice();
    }

    @Override
    public void confirmDeletePIN() {
        view.verifyPIN(REQUEST_DELETE);
    }

    @Override
    public void onClickModifyPIN() {
        view.verifyPIN(REQUEST_MODIFY);
    }

    private void deletePIN() {
        LogUtil.d(TAG, "confirmDeletePIN: deleting PIN");
        PINManager.clearPIN();
        view.setPINPrefText(R.string.pref_summary_pin_lock_unset);
        view.setFingerprintPrefEnabled(false);
        view.setPINDependentPrefsEnabled(false);
    }

    private void modifyPIN() {
        view.recordPIN();
    }
}
