package es.usc.citius.servando.calendula.scheduling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import es.usc.citius.servando.calendula.CalendulaApp;
import es.usc.citius.servando.calendula.util.LogUtil;

public class AlarmReceiver extends BroadcastReceiver {
    private final String TAG = "AlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (CalendulaApp.disableReceivers) {
            return;
        }

        AlarmIntentParams params = AlarmScheduler.getAlarmParams(intent);

        if (params == null) {
            LogUtil.w(TAG, "No extra params supplied");
            return;
        } else {
            LogUtil.d(TAG, "Received alarm: " + params.action);
        }

        Intent serviceIntent = new Intent(context, AlarmIntentService.class);
        AlarmScheduler.setAlarmParams(serviceIntent, params);
        AlarmIntentService.enqueueWork(context, serviceIntent);
    }
}
