package es.usc.citius.servando.calendula.util.stock;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;

import org.jetbrains.annotations.NotNull;

import es.usc.citius.servando.calendula.CalendulaApp;
import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.activities.MedicinesActivity;
import es.usc.citius.servando.calendula.persistence.Medicine;
import es.usc.citius.servando.calendula.util.IconUtils;
import es.usc.citius.servando.calendula.util.LogUtil;

public class StockDisplayUtils {
    private static final String TAG = "StockDisplayUtils";
    private static final int MAX_DAYS = 21;

    public static String getReadableStockDuration(StockCalculator.StockEnd estimatedEnd, Context ctx) {
        if (estimatedEnd instanceof StockCalculator.StockEnd.OnDate) {
            LogUtil.d(
                    TAG,
                    "updateStockText: estimated end date is " + ((StockCalculator.StockEnd.OnDate) estimatedEnd).getDate().toString("dd/MM")
            );
            if (((StockCalculator.StockEnd.OnDate) estimatedEnd).getDays() < MAX_DAYS) {
                return ctx.getString(R.string.stock_enough_for_days, ((StockCalculator.StockEnd.OnDate) estimatedEnd).getDays());
            } else {
                return ctx.getString(
                        R.string.stock_enough_for_weeks_days,
                        (((StockCalculator.StockEnd.OnDate) estimatedEnd).getDays() / 7),
                        ((StockCalculator.StockEnd.OnDate) estimatedEnd).getDays() % 7
                );
            }
        } else if (estimatedEnd instanceof StockCalculator.StockEnd.OverMax) {
            return ctx.getString(R.string.stock_enough_for_upper_limit);
        }
        return null;
    }

    @SuppressLint("StringFormatMatches")
    public static void showStockRunningOutDialog(final Context context, final Medicine m, Long days) {
        String msg = context.getString(
                R.string.stock_running_out_dialog_message,
                Integer.parseInt(m.getStock().toString()),
                m.getPresentation().units(context.getResources(), Double.parseDouble(m.getStock().toString())),
                m.getName(),
                days
        );

        new MaterialStyledDialog.Builder(context)
                .setTitle(context.getString(R.string.stock_running_out_dialog_title, m.getName()))
                .setStyle(Style.HEADER_WITH_ICON)
                .setIcon(IconUtils.icon(context, m.getPresentation().icon(), R.color.white, 48))
                .setHeaderColor(R.color.android_orange_dark)
                .withDialogAnimation(true)
                .setDescription(msg)
                .setPositiveText(R.string.manage_stock)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog materialDialog, @NonNull @NotNull DialogAction dialogAction) {
                        Intent i = new Intent(context, MedicinesActivity.class);
                        i.putExtra(CalendulaApp.INTENT_EXTRA_MEDICINE_ID, m.getId());
                        context.startActivity(i);
                    }
                })
                .setNeutralText(R.string.tutorial_understood)
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        dialog.dismiss();
                    }
                })
                .show();

    }
}
