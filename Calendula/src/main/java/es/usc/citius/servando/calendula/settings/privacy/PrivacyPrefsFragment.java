package es.usc.citius.servando.calendula.settings.privacy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.j256.ormlite.stmt.query.In;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;

import org.jetbrains.annotations.NotNull;

import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.pinlock.PinLockActivity;
import es.usc.citius.servando.calendula.pinlock.fingerprint.FingerprintHelper;
import es.usc.citius.servando.calendula.settings.CalendulaPrefsFragment;
import es.usc.citius.servando.calendula.util.IconUtils;
import es.usc.citius.servando.calendula.util.LogUtil;
import es.usc.citius.servando.calendula.util.PreferenceKeys;

public class PrivacyPrefsFragment extends CalendulaPrefsFragment<PrivacyPrefsContract.View, PrivacyPrefsContract.Presenter>
        implements PrivacyPrefsContract.View {
    private final String TAG = "PrivacyPrefsFragment";

    @Override
    public PrivacyPrefsContract.Presenter getPresenter() {
        return new PrivacyPrefsPresenter(
                new FingerprintHelper(getContext())
        );
    }

    @Override
    public int getFragmentTitle() {
        return R.string.pref_header_privacy;
    }

    private Preference pinPref() {
        return findPreference(PreferenceKeys.UNLOCK_PIN.key());
    }

    private SwitchPreference fingerprintPref() {
        return (SwitchPreference) findPreference(PreferenceKeys.FINGERPRINT_ENABLED.key());
    }

    private ListPreference pinTimeoutPref() {
        return (ListPreference) findPreference(PreferenceKeys.UNLOCK_PIN_TIMEOUT.key());
    }

    private SwitchPreference secureWindowPref() {
        return (SwitchPreference) findPreference(PreferenceKeys.SECURE_WINDOW.key());
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        LogUtil.d(TAG, "onCreatePreferences called");
        addPreferencesFromResource(R.xml.pref_privacy);

        pinPref().setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                getPresenter().onClickPINPref();
                return true;
            }
        });
        pinTimeoutPref().setSummary(pinTimeoutPref().getEntry());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.d(TAG, "onActivityResult() called");
        getPresenter().onResult(requestCode, resultCode, data);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(pinTimeoutPref().getKey())) {
            pinTimeoutPref().setSummary(pinTimeoutPref().getEntry());
        } else if (key.equals(secureWindowPref().getKey())) {
            if (secureWindowPref().isChecked()) {
                getActivity().getWindow().setFlags(
                        WindowManager.LayoutParams.FLAG_SECURE,
                        WindowManager.LayoutParams.FLAG_SECURE
                );
            } else {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
            }
        }
    }

    @Override
    public void recordPIN() {
        Intent i = new Intent(getActivity(), PinLockActivity.class);
        i.setAction(PinLockActivity.ACTION_NEW_PIN);
        startActivityForResult(i, PinLockActivity.REQUEST_PIN);
    }

    @Override
    public void showPINOptions() {
        new MaterialStyledDialog.Builder(getContext())
                .setTitle(getString(R.string.pin_actions_dialog_title))
                .setDescription(R.string.pin_actions_dialog_message)
                .setHeaderColor(R.color.android_green)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDialogAnimation(true)
                .setIcon(
                        IconUtils.icon(
                                getContext(),
                                GoogleMaterial.Icon.gmd_key,
                                R.color.white,
                                100
                        )
                )
                .setPositiveText(R.string.pin_actions_dialog_delete)
                .setNegativeText(R.string.pin_actions_dialog_modify)
                .setNeutralText(R.string.pin_actions_dialog_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        getPresenter().onClickDeletePIN();
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        getPresenter().onClickModifyPIN();
                        dialog.dismiss();
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void showConfirmDeletePinChoice() {
        new MaterialStyledDialog.Builder(getContext())
                .setTitle(getString(R.string.pin_delete_dialog_title))
                .setDescription(R.string.pin_delete_dialog_message)
                .setHeaderColor(R.color.android_red_dark)
                .setStyle(Style.HEADER_WITH_ICON)
                .withDialogAnimation(true)
                .setIcon(
                        IconUtils.icon(
                                getContext(),
                                GoogleMaterial.Icon.gmd_key,
                                R.color.white,
                                100
                        )
                )
                .setPositiveText(R.string.pin_actions_dialog_delete)
                .setNegativeText(R.string.pin_actions_dialog_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        getPresenter().confirmDeletePIN();
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull @NotNull MaterialDialog dialog, @NonNull @NotNull DialogAction dialogAction) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    @Override
    public void setPINPrefText(int pinPrefText) {
        pinPref().setSummary(pinPrefText);
    }

    @Override
    public void setFingerprintPrefEnabled(boolean enabled) {
        fingerprintPref().setEnabled(enabled);
    }

    @Override
    public void setPINDependentPrefsEnabled(boolean enabled) {
        pinTimeoutPref().setEnabled(enabled);
    }

    @Override
    public void showEnableFingerprintDialog() {
        //TODO
    }

    @Override
    public void verifyPIN(int requestCode) {
        Intent i = new Intent(getActivity(), PinLockActivity.class);
        i.setAction(PinLockActivity.ACTION_VERIFY_PIN);
        startActivityForResult(i, requestCode);
    }
}
