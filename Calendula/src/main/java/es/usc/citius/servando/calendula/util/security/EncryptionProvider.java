package es.usc.citius.servando.calendula.util.security;

public interface EncryptionProvider {
    String encrypt(String value);
    String decrypt(String value);
}
