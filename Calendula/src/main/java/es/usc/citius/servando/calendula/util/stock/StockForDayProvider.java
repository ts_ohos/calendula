package es.usc.citius.servando.calendula.util.stock;

import org.joda.time.LocalDate;

public interface StockForDayProvider {
    float stockNeededForDay(LocalDate date);
}
