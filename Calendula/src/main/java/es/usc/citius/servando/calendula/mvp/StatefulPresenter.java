package es.usc.citius.servando.calendula.mvp;

import android.os.Parcelable;

public interface StatefulPresenter<V extends IView> extends IPresenter<V> {
    Parcelable getState();
    void setState(Parcelable state);
}
