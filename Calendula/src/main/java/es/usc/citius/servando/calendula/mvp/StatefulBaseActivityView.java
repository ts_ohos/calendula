package es.usc.citius.servando.calendula.mvp;

import android.os.Bundle;

public class StatefulBaseActivityView<V extends IView , P extends StatefulPresenter<V>> extends BaseActivityView<V, P>{

    private final String SAVED_STATE_KEY = "STATEFUL_BASE_ACTIVITY_VIEW_STATE_BUNDLE";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null){
            outState.putParcelable(SAVED_STATE_KEY, presenter.getState());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        if (savedState != null){
            presenter.setState(savedState.getParcelable(SAVED_STATE_KEY));
        }
    }
}
