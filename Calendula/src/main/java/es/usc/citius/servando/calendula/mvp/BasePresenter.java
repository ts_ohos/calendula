package es.usc.citius.servando.calendula.mvp;

import org.jetbrains.annotations.NotNull;

import kotlin.reflect.KProperty;

public abstract class BasePresenter<V extends IView> implements IPresenter<V>{
    private ViewDelegate<V> delegate = new ViewDelegate<V>();
    public V view;

    public V getView() {
        return view;
    }

    public void attachView(@NotNull V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.delegate.view = null;
    }


    public boolean isAttachedToView(){
        return this.delegate.view != null;
    }


    private class ViewDelegate<V extends IView> {

        V view = null;

        protected V getValue(BasePresenter<V> pres, KProperty prop){
            if (view == null){
                throw new IllegalStateException("View cannot be null");
            }else {
                return view;
            }
        }


        protected void setValue(BasePresenter<V> pres, KProperty prop, V view) {
            this.view = view;
        }
    }
}
