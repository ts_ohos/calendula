package es.usc.citius.servando.calendula.settings.notifications;

import android.content.Intent;
import android.net.Uri;

import es.usc.citius.servando.calendula.mvp.IPresenter;
import es.usc.citius.servando.calendula.mvp.IView;

public interface NotificationPrefsContract {
    interface View extends IView {
        void hideStockPref();

        void requestRingtone(int reqCode, int ringtoneType, Uri currentValue);

        void setNotificationRingtoneText(String text);

        void setInsistentRingtoneText(String text);

        void setVisibleNotificationManagementPref(boolean visible);
    }

    interface Presenter extends IPresenter<View> {
        void onResult(int reqCode, int result, Intent data);

        void selectNotificationRingtone();

        void selectInsistentRingtone();
    }
}
