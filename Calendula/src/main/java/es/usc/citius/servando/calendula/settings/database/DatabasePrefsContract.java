package es.usc.citius.servando.calendula.settings.database;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.List;

import es.usc.citius.servando.calendula.mvp.IPresenter;
import es.usc.citius.servando.calendula.mvp.IView;

public interface DatabasePrefsContract {
    interface View extends IView {
        void setDbList(String[] dbIds, String[] dbDisplayNames);
        void showSelectedDb(String dbId);
        String resolveString(@StringRes int stringRes);
        void showDatabaseDownloadChoice(String dbId);
        void showDatabaseUpdateNotAvailable();
        Intent getIntent();
        void openDatabaseSelection();
        void askForDownloadPermission(String dbId);
        boolean hasDownloadPermission();
    }

    interface Presenter extends IPresenter<View> {
        void currentDbUpdated(String dbId);
        boolean selectNewDb(String dbId);
        void onDbDownloadChoiceResult(boolean result);
        void checkDatabaseUpdate(Context ctx);
        void onDownloadPermissionGranted(String dbId);
    }
}
