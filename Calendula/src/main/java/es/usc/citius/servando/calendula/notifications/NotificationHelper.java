package es.usc.citius.servando.calendula.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioManager;
import android.os.Build;

import java.util.Arrays;
import java.util.List;

import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.util.PreferenceKeys;
import es.usc.citius.servando.calendula.util.PreferenceUtils;
import kotlin.jvm.JvmStatic;

public class NotificationHelper {
    public static final long[] VIBRATION_PATTERN_MEDS =
            new long[]{1000, 200, 100, 500, 400, 200, 100, 500, 400, 200, 100, 500, 1000};
    public static final long[] VIBRATION_PATTERN_DEFAULT = new long[]{1000, 200, 500, 200, 100, 200, 1000};
    public static final long[] VIBRATION_PATTERN_DB = new long[]{0, 400};
    public static final long[] VIBRATION_PATTERN_NONE = new long[]{0L};

    /**
     * Intended for high-importance med notifications such as intake reminders.
     */
    public static final String CHANNEL_MEDS_ID = "calendula.channels.meds";
    public static final String CHANNEL_DEFAULT_ID = "calendula.channels.default";

    /**
     * Creates notification channels for the app (required from api 26 up).
     * This does nothing if the channels already exist: it's safe to call on every app startup.
     *
     * @param context a [Context], required for getting strings and the notification manager
     */
    public static void createNotificationChannels(Context context) {
        // don't do anything under api 26
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            //create meds channel
            String medChannelName = context.getString(R.string.channel_meds_name);
            String medChannelDesc = context.getString(R.string.channel_meds_description);
            NotificationChannel medChannel = new NotificationChannel(
                    CHANNEL_MEDS_ID,
                    medChannelName,
                    NotificationManager.IMPORTANCE_HIGH
            );
            medChannel.setDescription(medChannelDesc);
            //create other channel
            String defaultChannelName = context.getString(R.string.channel_default_name);
            String defaultChannelDesc = context.getString(R.string.channel_default_description);
            NotificationChannel defaultChannel = new NotificationChannel(
                    CHANNEL_DEFAULT_ID,
                    defaultChannelName,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            defaultChannel.setDescription(defaultChannelDesc);

            // register the channels
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannels(Arrays.asList(medChannel, defaultChannel));
        }
    }

    /**
     * Get notification vibration setting for the app.
     *
     * @param context a [Context], required for getting audioManager
     * @return a [Boolean], true if vibration setting is enabled; false otherwise
     */
    @JvmStatic
    public static boolean isNotificationVibrationEnabled(Context context) {

        int vibrationSettingInt = Integer.parseInt(
                PreferenceUtils.getString(PreferenceKeys.SETTINGS_NOTIFICATION_VIBRATION, "0"));
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        boolean result;
        switch (vibrationSettingInt) {
            case 1:
                result = audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0;
                break;
            case 2:
                result = audioManager.getStreamVolume(AudioManager.STREAM_ALARM) == 0;
                break;
            case 3:
                result = false;
                break;
            default:
                result = true;
        }

        return result;
    }
}
