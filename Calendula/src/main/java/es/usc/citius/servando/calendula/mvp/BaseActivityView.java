package es.usc.citius.servando.calendula.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;

import es.usc.citius.servando.calendula.CalendulaActivity;

public abstract class BaseActivityView<V extends IView,P extends IPresenter<V>> extends CalendulaActivity implements IView{

    P presenter;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView((V) this);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
