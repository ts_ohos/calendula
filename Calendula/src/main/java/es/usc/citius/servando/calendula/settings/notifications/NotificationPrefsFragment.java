package es.usc.citius.servando.calendula.settings.notifications;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.preference.Preference;

import es.usc.citius.servando.calendula.R;
import es.usc.citius.servando.calendula.settings.CalendulaPrefsFragment;
import es.usc.citius.servando.calendula.util.LogUtil;
import es.usc.citius.servando.calendula.util.PreferenceKeys;

public class NotificationPrefsFragment extends CalendulaPrefsFragment<NotificationPrefsContract.View, NotificationPrefsContract.Presenter>
        implements NotificationPrefsContract.View {
    private final String TAG = "NotificationPrefsFragm";

    private String getApplicationPackageName() {
        return getActivity().getPackageName();
    }

    @Override
    public int getFragmentTitle() {
        return R.string.pref_header_notifications;
    }

    @Override
    public NotificationPrefsContract.Presenter getPresenter() {
        return new NotificationPrefsPresenter(
                new RingtoneNameResolver(getContext())
        );
    }

    private Preference notificationPref() {
        return findPreference(PreferenceKeys.SETTINGS_NOTIFICATION_TONE.key());
    }

    private Preference insistentNotificationPref() {
        return findPreference(PreferenceKeys.SETTINGS_INSISTENT_NOTIFICATION_TONE.key());
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        LogUtil.d(TAG, "onCreatePreferences called");
        addPreferencesFromResource(R.xml.pref_notifications);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference.getKey().equals(PreferenceKeys.SETTINGS_NOTIFICATION_TONE.key())) {
            getPresenter().selectNotificationRingtone();
            return true;
        } else if (preference.getKey().equals(PreferenceKeys.SETTINGS_INSISTENT_NOTIFICATION_TONE.key())) {
            getPresenter().selectInsistentRingtone();
            return true;
        } else if (preference.getKey().equals(PreferenceKeys.SETTINGS_NOTIFICATION_MANAGEMENT.key())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                        .putExtra(Settings.EXTRA_APP_PACKAGE, getApplicationPackageName());
                startActivity(intent);
            }
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onResult(requestCode, resultCode, data);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //noop
    }

    @Override
    public void requestRingtone(int reqCode, int ringtoneType, Uri currentValue) {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        // show default and silent ringtones
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);

        // select ringtone type (alarm or notification)
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, ringtoneType);

        // set default element
        Uri defaultUri;

        if (ringtoneType == RingtoneManager.TYPE_ALARM) {
            defaultUri = Settings.System.DEFAULT_ALARM_ALERT_URI;
        } else {
            defaultUri = Settings.System.DEFAULT_NOTIFICATION_URI;
        }

        intent.putExtra(
                RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI,
                defaultUri
        );

        // set the current value
        intent.putExtra(
                RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
                currentValue
        );

        startActivityForResult(intent, reqCode);
    }

    @Override
    public void hideStockPref() {
        Preference preference = findPreference(PreferenceKeys.SETTINGS_STOCK_ALERT_DAYS.key());
        preference.setEnabled(false);
        preference.setVisible(false);
    }

    @Override
    public void setNotificationRingtoneText(String text) {
        notificationPref().setSummary(text);
    }

    @Override
    public void setInsistentRingtoneText(String text) {
        insistentNotificationPref().setSummary(text);
    }

    @Override
    public void setVisibleNotificationManagementPref(boolean visible) {
        Preference preference01 = findPreference(PreferenceKeys.SETTINGS_NOTIFICATION_TONE.key());
        preference01.setEnabled(!visible);
        preference01.setVisible(!visible);

        Preference preference02 = findPreference(PreferenceKeys.SETTINGS_NOTIFICATION_VIBRATION.key());
        preference02.setEnabled(!visible);
        preference02.setVisible(!visible);

        Preference preference03 = findPreference(PreferenceKeys.SETTINGS_NOTIFICATION_MANAGEMENT.key());
        preference03.setEnabled(visible);
        preference03.setVisible(visible);
    }
}
