package es.usc.citius.servando.calendula.settings.notifications;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;

import java.lang.ref.WeakReference;

import es.usc.citius.servando.calendula.R;

public class RingtoneNameResolver {
    private Context context;

    public RingtoneNameResolver(Context context) {
        this.context = context;
    }

    private WeakReference<Context> contextRef = new WeakReference(context);

    public String resolveRingtoneName(Uri uri) {
        if (uri == null) {
            return contextRef.get().getString(R.string.ringtone_none);
        } else {
            return RingtoneManager.getRingtone(contextRef.get(), uri).getTitle(contextRef.get());
        }
    }
}
