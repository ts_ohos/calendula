package es.usc.citius.servando.calendula.util.security;

public class NoopEncryptionProvider implements EncryptionProvider{
    @Override
    public String encrypt(String value) {
        return value;
    }

    @Override
    public String decrypt(String value) {
        return value;
    }
}
